// Class to represent a row in the seat reservations grid
function SeatReservation(name, initialMeal) {
    var self = this;
    self.name = name;
    self.meal = ko.observable(initialMeal);
    self.formattedPrice = ko.computed(function(){
        var price = self.meal().price;
        return price ? "$"+ price :"complimentry"
    });
}

// Overall viewmodel for this screen, along with
function ReservationsViewModel() {
    var self = this;

    // Non-editable catalog data - would come from the server
    self.availableMeals = [
        { mealName: "Standard (sandwich)", price: 12.95 },
        { mealName: "Premium (lobster)", price: 34.95 },
        { mealName: "Ultimate (whole zebra)", price: 0}
    ];

    // Editable data
    self.seats = ko.observableArray([
        new SeatReservation("Steve", self.availableMeals[1]),
        new SeatReservation("Bert", self.availableMeals[0])
    ]);

    self.addRow = function(){
      self.seats.push(new SeatReservation("Shahi" self.availableMeals[2]));
    };

        self.removeSeat = function(seat) { self.seats.remove(seat) }

}

ko.applyBindings(new ReservationsViewModel());
